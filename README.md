Ce que j'ai suivi...

# Mardi

Matin : keynote : https://www.opensourcesummit.paris/CONFERENCE+PLENIERE_168_5758.html

Après-midi :

GOVERNANCE : https://www.opensourcesummit.paris/GOVERNANCE_168_5740.html

Business models & valorisation
14H00 - 16H00 // SALLE LE MONTAGE

Mise en oeuvre de stratégies Open Source
16H30 - 18H30 // SALLE LE MONTAGE

# Mercredi

Matin :

CLOUD DEVOPS :

Automation & DevOps
09H30 - 11H00 // SALLE LONG METRAGE

GOUVERNANCE : https://www.opensourcesummit.paris/GOVERNANCE_168_5740.html

Gouvernance open source
11H00 - 12H30 // SALLE MIXAGE

Après-midi : SÉCURITÉ : https://www.opensourcesummit.paris/SECURITY_168_5737.html

L'usage open source : initiatives et retours d'expérience
14H00 - 17H30 // SALLE STUDIO PHOTO

