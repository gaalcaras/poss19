Les aventures picaresques de l'Open Source à l'Insee : tableau de mœurs
numériques en mutation : Arnaud Degorre & Benoît Werquin, INSEE

Présentation des deux mecs de l'INSEE maintenant.

Deux statisticiens s'essaient aux humanités en racontant une histoire
picaresque.

INSEE admin publique, direction économie et des finances, mais une certaine
autonomie pour connecter et diffuser de l'information sur l'économie et la
société. Indépendance actée dans la loi et renforcée par la création d'une
commission de la stat publique pour éviter influence des changements
politiques.

"An open-minded country".

Avant il fallait payer les bases, 2010s ce n'est plus payant, maintenant ils
essaient de faire de l'open data. Tournant avec le répertoire de SIREN => tout
est versé en open data, service publique de la donnée.

Au sein de l'institut, il y a des "tribus primitives". Une certaine forme de
culture informatique. ENSAE/ENSAI : utilisateurs avancés qui aiment bien
bricoler, biduler => histoire particulière au sein de l'INSEE.

Open source joue un rôle particulier au sein de l'INSEE. Gratuit : donc c'est
pas bien.

Premier chapitre : comment dîner gratuitement. Grosse adhérence à Oracle, tous
les admistrateurs étaient formés à Oracle. Début des années 2000s :
architectures web, autour de Java. Outil de dev libre avec Eclipse. Plus dans
les bureaux locaux, régionaux : pile LAMP. 2010s : sortir progressivement
d'Oracle vers PostgreSQL.

Fermeture de casernes à Metz => créer de l'emploi => INSEE crée un centre
statistique à Metz. À l'époque, beaucoup de choix comparés et choix de Debian
(centOS) => plutôt un choix en terme de structure communautaire, plus de Debian
en FR. Pas des choix techniques mais sur capacité à mobiliser une communauté.
+ Virtualisation (mais c'est VmWare, tous les postes sont sous Windows).

La question qui se pose aujourd'hui : est-ce qu'il faut pas revenir vers des
distros mieux supportées comme RHEL.

Lab Datacenter propulsée sur une version community, ils vont peut-être acquérir
la license DCOS entreprise.

Chapitre 2. Les utilisateurs

Spontanément, les utilisateurs n'adoptent pas libre office ou open office
(malgré encouragements) car ils ont plein de macros Excel VBA.

2016 : libre office devient un standard.
2017 : ms office n'est plus installé sur les machines.
2019 : ms office est retiré de (presque) toutes les stations.

C'est la transition encore aujourd'hui la plus compliquée.

Transition de logiciel statistique en tant que tel. SAS. Les sages jugent que
SAS c'est mieux, plus sûr, très bien maîtrisé. R arrive petit à petit. Volonté
de sortir de la dépendance vis à vis de SAS, risque financier potentiel. Plus
possibilité de bénéficier des paquets R.

Acteurs clés : les hommes de l'ombre, qui créent des réseaux locaux de useR
groups.

Leçons clés :

- ne pas basher les technos propriétaires, ils ont perdu beaucoup de temps
  à essayer de montrer que R > SAS => pas la bonne approche.
- dette technique à cause des chaînes statistiques sous Excel.
- ne pas vendre open source sur sa gratuité, c'est "cheap", le public ne paie
  plus des vraies solutions comme Oracle.

Chapitre 3. Faire de l'OS ?

- Pression externe principalement des institutions publiques et des
  associations comme "Ouvre boîte".

Deux tentatives fructueuses :

- plateforme Adullact (simulation de réforme fiscale)
- open source outil de collecte de l'information, un "produit" pour créer des
  questionnaires et créer automatiquement des instruments de collecte
  (téléphone, tablette, papier en face à face). Pogues et Eno sur Github.

[là c'est l'informaticien qui parle, chemise jean]

Les questionnaires de l'INSEE sont longs, très compliqués, on a le souci de
l'exhaustivité. "C'est presque une torture". À la base c'était interne, il était
pas question de faire de l'OS.

Difficulté de standardiser le questionnaire : pas facile, car chaque enquête
a son informaticien avec sa techno. L'open source va nous aider dans notre
stratégie de réutilisation / standardisation en interne, "c'est un peu tordu
vous voyez". L'idée : les autres instituts européens ou autres l'utilisent pour
convaincre les gens en interne (les cadres).

Coder en anglais, plus en français.

POGUES : lui direct en OS.

"On a réussi à mutualiser des efforts entre des directions qui d'habitude ne se
parlent pas beaucoup, enfin ça on n'est pas censé le dire".

Très peu de contributions externes, car c'est une toute petite communauté avec
des outils très complexes. Pour l'instant c'est surtout de la documentation et
des retours d'expérience.

[statisticien, polo cravate]

C'est plus compliqué : changement de managament, passer à un contrôle par les
pairs ce que les managers n'arrivent pas toujours à intégrer.

Depuis récemment : la valeur de l'INSEE c'était la donnée. Maintenant c'est
ouvert. Puis les algos, mais ça aussi on l'ouvre, donc c'est plus ça. Donc
l'INSEE c'est devenu [ou ça devrait devenir ?] un institut de certification
qualité.

Double culture de management au sein de l'institut : personnel militaire et
personnel d'ingénieur de formation. Technicité forte et goût du contrôle de ce
qui est fait.

