Yoav Kutner, do not be afraid to be forked [keynote]

[en anglais]

**Yoav Kutner**. Le mec a tout co-fondé, Magento et autres. Akeneo PIM
(Nantes) : gestion de projets open source ? => à fouiller.

We didn't notice, but we relied on open source stack to be competitive.

Beaucoup de remarques du type : "I don't know if you remember this guys" parce
qu'il parle de "vieilles" technologies.

"It was a true community projet" => osCommerce (Open source commerce).
Contributinos de sa boîte : séparation control // view [classique pour les
frameworks web des 2000s], etc.

"Sell to our customers", build what our customers need. Ça allait avec leur
business model parce qu'ils vendent des services et pas un produit logiciel.
Division du temps, payé par les clients pour osCOmmerce, le week-end sur le
projet open source qui doit le remplacer.


"Like every open source project, we had a community that started following us".
Mais des membres de la communauté, une bonne communauté, mais des membres ont
essayé de faire les choses différemment [donc ils ont forké ?]. "they were
identifying themselves as Magento developers, no longer as PHP developers, we
were prooud of that of course". Ils avainet beaucoup de problèmes au début
parce que les contributeurs étaient souvent mauvais dans leurs premières
contributions [ il le dit plus diplomatiquement en anglais ].

Ils ont travaillé avec une autre entreprise pour faire des web services et les
intégrer à Magento, et avant le lancement, ils laissent tomber pour se
concentrer sur leurs acitivés commerciales. Direct contributions we were not
excited about, because we don't want to rely on third parties developers. Ils
maintiennent le core du logiciel, contributions se font pour la sécurité
/ patches de bugs. Solution => extensions, add-ons.

"We are in the business of leaving holes" (Roy Rubin) [apparemment c'est une
phrase inspirante pour Yoav] => ils laissent des fonctionnalités vides pour
faire de la place pour des exntensions.

"Investing in the community", cercle vertueux entre profits pour la communauté
(l'industrie dans ce cas ?) et pour eux en tant qu'entreprise.

We start hearing more and more about forks in our community. We were extremely
worried about it, beacuse a fork could become more successful than our version.
Essayer d'aprrendre des forks précédents pour éviter le fork : en gros, écouter
ce que dit "la communauté" pour "make our project even better".
