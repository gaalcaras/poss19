CNLL / Syntec numérique / Systematic / Tecknowlogy

Stéfane Fermigier, CNLL
Philippe Montargès, Systematic
Marc Palazon, Syntec Numérique, Président Smile

Table ronde étude de marché sur l'open source. Liste des speakers sur slide.
Animée par Amel Charleux (?) qui est doctorante en sciences de gestion, sur
open source dans entreprises. Comment des concurrents collaborent entre eux ?

Étude sur 117 organisations en Europe.

Résultats.

Mec CNLL : Un marché très mature pour l'OS. Un volume important pour taux de
croissance [mais je comprends pas ce qu'ils mesurent, nombre de projets ?
millons d'euros ? Mais mesurés comment ?]

En France : +9% chaque année => 2023, Vers 12% de l'OS. 7 milliards €.

Mec Tecknowlogy [?] : point clé, l'open source est devenu mainstream dans le
digital et les stratégies d'organisation et d'innovation. L'Os était auparavant
assimilée à des stratégies low cost et pas très qualittavise par rapport au
propriétaire. Or maintenant c'est le contraire, l'OS est création de valeur.
Inversion : à l'origine, poussé par "les couches basses du SI", puis les CTO.
Mais maintenant, de plus en plus de ceo et de COO sponsorisent l'OS au sein des
directions générales.

Slide raisons d'adopter l'OS :

1. customisation
2. indépendance
3. coûts [alors qu'il y a encore quelques années il était en tête]

C'est "le couteau suisse numérique", c'est "la boîte à outils pour l'innovation".

Tout projet qui se lance maintenant embarque toujours des solutions OS.

Enjeu fort pour l'Europe, pour "retrouver un leadership numérique européen".

Le mec suivant : lui il utilise le terme de souveraineté.

Au niveau global de l'Europe [UE ?], une étude du marché OSS en 2020.
