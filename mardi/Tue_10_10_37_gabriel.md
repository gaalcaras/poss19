**Roundtable** : Amandine, Fabien et Oav qui vient de terminer son talk.
Fabien Potencier, SensioLabs et Symfony
Zeev Suraski, Zend
Amandine Le Pape, Matrix.org foundation, New Vector
Yoav Kurtner, Oro Inc

*How to create a successful business model with open source?*

Amandine, New Vector, projet open source Matrix => control who's owning the
conversation. NV créée pour "support" Matrix et son équipe.

Fabien, entrepreneur, Sensio. [lui est en costume]. It's really hard to
monetize open source, I can tell you. [Il a créé Symfony ?]

Zev [?], (en hoodie), a participé à la création de PHP (v3 [ça fait rire le mec
à ma gauche]). Le choix de l'open source était évident parce que PHP était open
source (quand il créée sa boite, Zenv (?)). A quitté Zenv, la start-up pour
qui il travaille est encore secrète donc il ne dit pas le nom [!!].

Petite blague du modérateur : "maybe we can organize a roundtable about PHP"
[ça fait pas beaucoup rire, probablement parce que PHP est devenu si peu cool
en dix ans]]

Yoav : MIT licence to "encourage users to disrupt the industry".

*Do you not agree that open source is not only about business but also about
values?*

Fabien : *open source* is not only about code, the licence is not a goal in
itself. First thing : documentation. If people are not able to use your
software, it doesn't work => that's a big question. "How to build a community
around that" => c'est encore plus complexe aujourd'hui. il y a 20 ans, il n'y
avait pas Github, moins de projets open source. Aujourd'hui, trop de projets
open source => "you have to stand out". documentation, onboarding process,
diverdiversity => new subjects.

Yoav : *it's a strategy that we believe it will be more successful*

Amandine : pas d'accord, quand on a créé Matrix, just the open source side of
things, not business. Endox (incubateur) R&D project
for 3 years, building community, Au bout de 3 ans, d'autres entreprises
commencent à faire de la $$$ avec Matrix : pourquoi pas eux ? *We want to grow
the ecosystem*. Ils utilisent la licence la plus "open" : Apache [ïntéressant
du ponit de vue de la politique des licences].

Zev. *Community* : two facets, users and contributors, but if you're the only
company developing it => it's going to be difficult to have a good open source
project. You have to have outside contributions.

*Sometimes it's hard to work with your communty right?*

**Fabien**. Communauté très large : difficule car beacuop de buts très
différents. *At some point, you need someone to say yes or no and take
decisions*. You not only need to fork the code but also the community. Only one
successful fork in PHP, that's Joomla. You can fork the code, but not the brand
(can't use PHP for a fork of PHP).

**Yoav** : commercial company, open source asa a strategy, millions of dollars
in developing it. Marketing to develop awereness of the company => open source
as a communication strategy. Trouver d'abord les entreprises pour les
convaincre d'utiliser Magento, puis trouver des dévs en leur montrant qu'il
y a du taf avec leurs frameworks. Ensuite, Magento est tellement plus gros,
quel les forks se font bouffer car ils copient juste ce qu'ils font plus vite.

**Amandine** : *In terms of makers vs takers*. A community is a validation of
your project, you're doing things right. We ended up in the same situation than
Yoav but starting from the bottom up.

*What is the best way to build an open source company?*

**Amandine** : no best way, ça dépend.

[marrant, l'interviewer essaie d'avoir des réponses sur le business model mais
personne ne répond et surtout personne ne parle d'argent].

**Yoav** . Dual licensing is the best **except for the cloud today**, because
othervwise *you end up competing with your ecosystem*. The business model that
scaled well for us was the dual licensing model, Devenus une *software company*
alors qu'au début *service company*.

**Zev** : open core model => one of the most difficult. Today, Amazon is the
most successful company using open source, they make huge amonuts of money
using software they didn't write and didn't contribute to much. It's a chicken
and egg situation, because you need a successful project to create a successful
project. So you need to have a successful

**Fabien** : support is the worst business model, *never do that, it doesn't
scale* [because open source scales very rapidly]. Symfony started out as
a marketing project for a web agency, but not a way to make money. Then
realized that they were pouring a lot of money into it, so need later to
monetize it. It's not free.

**Amandine** : open source => huge ecosystem => huge amounts of money to be
made => vs proprietary => limited silos => less money to be made.

[pub Symfony sensio labs]
