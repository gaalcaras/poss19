Comment l'open source, par sa nature, peut-il générer du business ? : Kevin
Dejour, Oro Inc

[notes prises sur téléphone car plus de batterie, donc prise de note un peu
moins dense]

Dejour Kevin

14h41

Comment ça peut générer du business ?

Pro inc, e-commerce b2b.

Petit rappel sur ce qu'est l'Open Source. Tout se trouve en open source.

On passe en anglais.

Le mec dit des trucs pas exacts je crois (raspberry pi serait open ?)

Oro platform is based on open source solutions, recursively.

Two other companies akeneo and marello use their platform Oro platform.

All of that is free, so can it generate business?

Licence :
- dual license system (trucs classiques), version entreprise plus performante,
  support, etc.

Services :
- expertise training audit

Intégration :
- Customisation (logo, New design) add functionalities to match their own
  processes internally but in Europe, most editors don't do integration, so
  they have partners to make the integration

Partners :
- easier to make interconnect services between similar technical systems build
  a big platform, then it reinforces the big platform 

=> C'est ce qu'on appelle un écosystème

Question sur le support : Le support ça ne marche pas en fait.

Le mec : Two kinds of support :
- core product (license to access the code, you do the support) external
  functionalities (you need other people to do it)

Le mec revient à la charge : support doesn't scale parce qu'on doit embaucher
des gens

Un cas de magento : support matters, but it's different b2b (moins de clients)
v b2c (trop de clients)

Mec INSEE : talk about using but not Producing open source ; connectors are
proprietary ? How do you do money [rires genes clairement le mec a touché un
nerf sensible]

Presenter : on trouve toujours des gens pour commencer un projet gratos dans la
tech, on trouve de l'argent après. On the connectors : they are a kind of open
source, open source to commercial.

Question : do you see the CE to sell more Enterprise edition ?

P : we need both. Keep both in sync, ie features of the community make it to
the . 

Question : my question is more about the why, do you make open source or do you
make money?

Mzc magento : you answered the question yourself. You said company.

P : we're a company so we need to make money. But we also need the community,
to have more developers.

Amel : Can we say that community is the path to make more money?

P : community is a part of the product,

Question sgf : do you have external contributors ?

P : yes, 

Q : what kind?

Q : how do you convince customers to contribute to your software ?

P : you say you will integrate it to the core product

Il ne sait pas quelle licence ils utilisent. Ne connait visiblement pas les
contributors agreement.

P : we use GitHub.

Mec xwiki : Mit is very free, you can change the license and don't ask anyone.
Vs Strong license you need a cla. With free license, you can stop open source
the project, example Snips acquired by Sonos. You have to check the intentions.
It's a problem of theses licenses

Amel interrompt la discussion, ça commence à chauffer un peu.




