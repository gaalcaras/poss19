**Prix des acteurs du libre** [keynote]

[Un peu plus de gens dans la salle. Mais beaucoup plus d'hommes] Slide des jurys.

Prix du meilleur projet :

- dynamique communautaire
- qualité du porteur ("mais ça c'est un peu chaud")
- nombre d'utilisateurs / contributeurs

Gagné par : ICIJ => projet datashare. deux femmes (30 ans ou moins, une en
hoodie front end dev, product owner) montent sur scène avec un homme (plus 40
je dirais, backend dev, jean / t-shirt).

La femme qui parle (Soline Ledésert) : "le projet peut être utilisé pour faire
autre chose que du journalisme, et c'est gratuit".

Prix de la meilleure strat OS :

- Importance OS dans la stratégie globale de la boîte
- Historique / longévité
- Engagement communautaire

Gagné par... La SNCF. [deux hommes ~ 40 ans montent sur scène, on dirait des
cadres plus que des devs : chaussures en cuir, polo, veste]. Il manque un
contributeur OpenStreet Map [cf thèse de Clément Marquet]. Travail sur
PostGresql notamment. Libération de toc (chat bot oui.sncf) "reverser la
solution à la communauté" pour améliorer "l'ia conversationnelle".

Animateur : critère de la calvitie n'a pas été utilisé pour le prix strat OS.

Prix de l'innovation : 

- qualité du porteur ["ça c'est un classique"]
- clarté de la description ("on a reçu des dossiers un peu durs à comprendre")
- l'utilité
- l'historique d'utilisation

Gagné par... AxiomTeam, Cédric Dumond [jean + t-Shirt]. Monnaie libre, projet
créé il y a deux ans. Quelques milliers en France à s'en servir, pas besoin de
l'acheter, c'est une "monnaie juste".

Prix du développement international :

- partenaires / bureaux en place
- nombre de clients
- langues locales implémentées
- qualité du porteur [????????? mais c'est quoi ??]

Gagné par PrestaShop. Antoine Thomas, Developer Advocate [gère les relations
avec les dévs et l'OS].
