Comment financer son projet Open Source ? : Ludovic Dubost, Xwiki [à la base ce
talk était prévu pour le mercredi je crois]

Xwiki

Ludovic dubost

15h14

Présentation about having the company supporting your job as an open source
developer, not to make money primarily.

2 ME de revenus.

Investors are employees or ex employees. No external or financial shareholders.
He's the majority shareholder.

150 clients, 7000 installs. Large organizations (Amazon, SNCF, smaller
companies)

Cryptpad : service cloud chiffré, lutter contre les invaders of privacy

Libre :
- beaucoup de code produit par les gros Start-ups use open source as a freemium
  strategy, even though developers might in the company. Exploiting open source
  as freemium strategy, it works really well

Just a part of the technology industry financing the open source.

Support paid by the hour. The only pay when they have a need. They will never
pay you to maintain it.

When you do support, sell support not by the hour so that you can invest in the
open source project.

You can't finance end-user application or it's more difficult.

First thing : you need a good product AND a good community (the first part of
the freemium strategy). You have to convince people. The early adopters is not
necessarily the one that will pay for the product in the end, so your community
might mislead you about what to do to make money.

Refuse financial investors because open source will become a secondary concern.

Gsoc : façon de recruter un dev en Roumanie.

Support enterprise : difficult at the beginning because it's not critical
software. But then sell it for multi year support. Include paying modules in
the support package and say that you will pay more for one year (and not that
you will charge less for multi year). You have to explain to customers that
they're paying for r&d.

You have to anticipate the question : Do we actually have to pay? That the
business manager will ask. If the answer is no If the answer is no (often the
case), then they won't pay => modules payants

(Son discours fait rire sogé et Brad)

Do service at the beginning, but it will be difficult to scale and customers
won't pay you by the hour. Forfait vs régie, régie is much better. The majority
of the support contract comes from the service contract. Service is a good deal
to make a feature, you can split the cost between customers. Amazon sponsored
150k of dev in a year.

Raise the price for non support users => create a discussion about open source
software. It's a way to explain the open source model.

We provide the same price whether you use cloud or not. The lower version of
cloud has no human support.

Don't sell an Enterprise edition, but sell extensions. The extension is open
source on GitHub, but on the market place, you pay to install it. So they have
to make the technical effort if they want it to be free.

Open source : you have to accept the value of competition. That means that
someone else could come and provide exactly the same service.

Explain that you have to explain that if you don't pay for the open sormurve,
it won't exist anymore.

1% of the userbase pays.

Lgpl.  Cryptpad is a stronger license. So if competitor, they have to
contribute back. No cla to create trust with customers that they can't take the
software proprietary.

