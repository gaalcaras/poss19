Publier du code Open Source dans une banque : mission impossible ? : Fabien
Baligand et Damien Trouillet, Informatique CDC

Baligand, Trouillet [société informatique de la Caisse Des Dépôts]

*Publier du code open source dans une banque : mission impossible*

[beaucoup de gens sont partis à 17h30, probablement question de transport => il
reste seulement une quinzaine de personnes,deux femmes dont Amel qui est track
leader].

Petit jeu au début : pose des questions sur qui bosse dans une banque, qui
utilise de l'OS et qui produit de l'OS. En gros 70% de l'assistance répond oui
aux trois, surprise (surtout pour la dernière et rires).

Damien [on dirait laurent deutsch avec une barbe]: "je suis grand fan de
domotique, au grand dam de ma femme" [oh yeah]

L'autre mec : la contribution open source a commencé chez lui (pour éviter les
problèmes juridiques) puis l'a apporté dans l'entreprise.

Pas besoin de vraiment convaincre ses boss, c'est juste qu'ils ne savaient pas
que ça existait + il leur présente comme un moyen pour la banque de contribuer
à l'innovation : le directeur général dit direct "C'est quoi qui bloque ?".

Tournées des directions opérationnelles :

- pour les RH : facile de convaincer en disant recruter des talents (les
  meilleurs devs font de l'OS)
- juridique : premier truc à regarder, est-ce que github est gratuit même si
  t'es une entreprise ? Licences, Apache 2.0 [la licence la plus pratique pour
  ceux utilisent, "c'est open bar" ; pour les producteurs moins de
  responsabilités] ; pour les droits : le code produit par les informaticiens
  appartient à la boîte, donc le DG doit signer un accord pour l'autoriser
- sécurité : ils avainet déjà envoyé des avertissements à des ingénieurs qui
  pushaient sur Github depuis la banque, donc appréhension : mais là, en
  proposant un cadre légal, ça marche [ce que je comprends en gros c'est plus
  illégal de publier du code open source donc ils ont moins de taff et
  d'angoisse]

Guidelines sécurité : attention aux mdp, clés SSH, certificats, urls internes
qui ne doivent pas traîner + du scan automatique de dépôt.

Accord de direction d'abord [il faut faire un simple email à la direction],
puis faire un audit du code [notamment sécurité + bonnes pratiques de code].

Premier code 2019 : EBAD (exécution des batchs à distance).

Git en interne.

Problèmes du premier projet :

- thème était payant et propriétaire
- normes internes qu'il faut revoir
- passer la CI / CD jenkins interne => faire plutôt avec Travis CI
- nettoyer le code [audit]
- code sensible, il y avait des APIs internes visibles, etc => il faut les
  enlever => passage vers plugin / modulaire.

Projets à venir : Ansible pour Windows, portail DevOps pour l'entreprise.

github.com/informatique-cdc
