Fabrice Dewasmes, *Projet Koda, l'ia et l'open source au service des sourds et
des malentendants*, directeur de l'innovation smile

Smile, projet pour les sourds malentendants.

[beaucoup de gens s'en vont pour Smile, je note, une bonne dizaine ce qui fait
beaucoup dans l'audience clairsemée]

Directeur de l'innovation chez Smile.

Extension pour Magento, Elastic Suite.

"Créer de la valeur", "créer quelque chose qui va être vendable". Mais valeur
au pluriel c'est mieux.

"si on est plus bold, on aide l'humanité" [il a vraiment dit ça]

**Poubelle intelligente**, c'est un exemple de **tech for good**.

"La majorité s'adapte rarement à la minorité", et pourtant il y a beaucoup de
personnes muettes, 7 millions de personnes : "c'est une belle minorité". Gna
gna, c'est dur d'assumer le poids de son handicap, aucune adaptation n'est
faite pour ces personnes au quotidien. Appli web pour traduire la langue des
signes. Beaucoup de technos open source : flux rtc pour le serveur, tensorflow,
keras et opencv sur le serveur. Le "device". Tous les ans des hackathons
internes appelés "fire starter". "On a voulu étendre cette communauté et donc
trouver des partenaires" pour avoir de la data, aller vers "cette communauté
LSF". Techno open source, Koda.
