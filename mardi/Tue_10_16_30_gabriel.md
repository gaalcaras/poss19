D'un programme gouvernemental... à un vrai développement communautaire, le
chemin de VITAM : Jean-Séverin LAIR, DINSIC

Vitam.

Il commence par dire qu'il ne sera binetôt plus directeur de ce programme =>
"vous verrez bientôt pourquoi".

Programme Vitam interministériel : backend infrastructurel commun pour les
archives de l'État (dont Défense).

[le mec parle avec un ton très blasé et ironique dès qu'il invoque
l'administration et la France, on sent qu'il n'en pense pas du bien : est-ce
qu'il vient de l'admin ? Haut fonctionnaire ? Du privé ?]

Pour arriver à faire quelque chose de collaboratif, il a fallu forcer les gens
à le faire.

Du Scrum.

Dès le développement, on s'est mis en mode contributif sur ce qu'on faisait :
enrichissement de doc, corrections de bugs,

Une communauté professionnelle "très forte" : des archivistes, une centaine de
participants très actifs en présentiel. Mais là c'est une implication très
personnelle. Exprimer leurs besoins pour être sûr qu'ils soient pris en compte

Acte III-bis : "commencer à créer une communauté en termes institutionnels" :
15 entités publiques. La V3 de vitam va sortir en mars 2020, lui il passe la
main. Mais une équipe agile reste au complet, et ils vont s'ouvrir beaucoup
plus. Jusqu'ici, les partenaires sont seulement des institutions publiques.
Dans notre club utilisateur, accepter des acteurs privés : c'est un autre axe
d'ouverture.

Maintenant ouverture à des contributeurs externes, le CEA voulait des
connecteurs S3 pour son système => verser sur Github. Société Locarchives,
ajouter des éléments de journalisation => verser une fonctionnalité
supplémentaire dans le système. Mettre en place une communauté de
contributions.

C'est un archiviste qui a pu embarquer les autres archivistes.

Comment on contractualise dans le public dans le développement Agile ? Cette
fonctionnalité est développée pour tant de points de complexité.

Beaucoup travailler sur les réseaux sociuax car ça marche bien avec les
archivistes.

Vitam pensé à la base comme un back office, donc infra, mais front office est
mis en place par les différents acteurs eux-mêmes. Les interfaces utilisateurs
seront développées plus directement en mode contributif.

Un peu de pub pour la fiche de poste. "travailler sur de l'open source en
méthode agile, ça peut être sympa".

Github ProgrammeVitam.

Questions portent surtout sur les langages utilisés. Java. Problème juridique
du JAVA 8, passage bientôt en Java 11. Beaucoup de questions sur le stack :
mongodb / elastic search. Pas de questions sur les salaires etc comme dans les
sessions précédentes (mais le public a changé, mec de xwiki plus là).

Question intéressante : le présentateur explique que les admins foncent pour
faire le logiciel, mais maintenant ils galèrent à mettre les données dedans
(parce qu'il faut manipuler les bases, refacto les solutions existantes pour
organiser les données, etc).

CLA agreement mais ici pour s'assurer que le code sera bien contribué. Bientôt
diffusion d'un règlement de communautés, surtout pour éviter que les gens
pensent que l'open source c'est juste envoyer son code et c'est fini.

Les partenaires : des institutions "qui ont beaucoup de volumétrie" (dont
huma-num au passage je remarque dans ses slides).

Nécessité de faire un back plutôt qu'une bdd commune : "c'était pas naturel
pour la culture et les armées de stocker leurs archives ensemble". Mais des
entreprises (manqué le nom, celle mentionnée plus haut je crois) proposent des
services payants en utilisant cette infrastructurel.

Discussion sur le stockage : filesystem, S3, swift, et récemment bandes
magnétiques.
