Guillaume Rincé, CTO MAIF [Keynote]

Maintenant on va parler de stratégie open source. CTO de la MAIF.

[les trois mecs de la MAIF devant moi filment avec leur téléphone]

"Démarche #openSource et contribution à un monde meilleur"

Mutualisme de la MAIF, valeur forte.

MAIF contributeur de communs, valeurs de performance (aux sociétaires) et
d'éthique (proposé des pièces de réemploi plutôt que des nouvelles).

"Notre engagement dans l'open source est avant tout sincère"

MAIF : "nous sommes également contributeurs de certaines technologies"

Maintenant ils franchissent le pas, chercher des briques qui n'existent pas =>
créer des nouvelles technos, et décision de les partager et "donc de liébérer"
nos technologies. Jan 2018 : otoroshi (API management, rendre accessible les
API poru les partenaires) ; isanami (activer des fonctionnalités à chaud) ; ça
continue, plus récemment Melusine (usine à mails, brique d'intelligence
artificielle pour pré traiter les mails avnt que les gestionnaires les
recoivent).

Blabla sur leur démerche "d'entreprise à mission", charte "raison d'être" :
"garantir un réel mieux commun".  Finalement quoi de mieux pour caractériser
l'open soruce ?

Beaucoup d'effets positifs de l'OS au quotidien :

- certains attendus, nos équipes trouvent beaucoup de satisfaction à faire des
  logiciels qui vont être utilisés ailleurs, importance de participer à des
  conférences, faire des émules, etc.
- attirer des talents : OS est attirant pour les recrues
- effets positifs inattendus : la manière dont on fabrique nos solutions : "mis
    à disposition de la communauté" : ne pas créer d'adhérence avec nos propres
    systèmes d'information. Autre élément, qualité du code open source, parce
    que les applis vont être regardées, "que vous allez les challenger". Autre
    bénéfice, coût de ces solutions "mais qui n'est pas quantifiable" qui est
    "sûrement moindre".

Démarche de gouvernance, pourquoi mettre en OS ou en propriétaire.


Github de la MAIF, lien sur la slide.

ISANAMI.

Exemples d'orgas qui l'utilisent : 

- Jyllands Posten (quotidien danois)
- OVH pour la Continuous Delivery

Mélusine : usine à mail. Machine learning mails en anglais. Le logo est une
sirène (? peut-être ?).

Problème des emails de plainte qui n'arrivent jamais à la bonne adresse email.
La plupart des librairies existantes sont bonnes en anglais mais pas en
français. Repérer le type de problème (contractuel / sinistre). Diviser par 40%
le temps de traitement des emails.

Annonce de daikoku complète otoroshi. "Allez tester la brique sur norte
Github". "votre démarche d'apisation". Possible de les monétiser (accès aux
APIs) avec daikoku.

Annonce des deux autres conférenciers (une tech usr mélusine, l'autre sur
gouvernance au sein de la MAIF). Premier meetup OSSbyMAIF fin janvier.
