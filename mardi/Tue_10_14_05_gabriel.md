Prévu : L'open road map de Tuleap : un modèle idéal pour financer le
développement d'un logiciel open source? Retour d'expérience sur 5 ans de
pratiques : Laurent Charles, Enalean

[j'arrive un peu au milieu]

L'intervenant n'a pas pu venir donc tour de table.

À droite, doctorant sur questions juridiques.

À gauche, in charge of marketing and co-development. 3d modelling. [je sais pas
qui c'est, je vais l'appeler Brad]

Amel : what's the size of the community ? 
R : I don't know, I know only the number of users, about 30000.
Amel : how many contributors
R : only one hundred, but I'd like to grow this number.

Société générale [jeune homme, veste, grosse barbe, fort accent indien en
anglais] : big bank we need security when adopting open source.

Sa collègue : project manager for the develepers. How to attract develepers
outside Société Générale.

Sylvia : exchanger smthg, help companies to scale, interesting with open source
to allow scaling. "But I'm not technical at all".

Antoine Guichard : private consultant, help to manage projects and
collaborations, projects that become OS. How to convince my clients how to do
it?

Veolia : lots of business units, we are thinking of applying the open source
principles within the companies, we have dev skills all around the world, but
they are working on their own issues instead of distributing them accross the
whole organization. Inner sourcing [pluseurs hochements de têtes]

Simon Constant [je vais parler en français], je faisais partie de la team
CozyCloud.

- how to attract contributors and users
- how to bring open source into the culture of the company?

INSEE : we don't think we have the best practices, we just push the code on
Github, at the beginning we have no idea what is open source. We are using the
tools of open source, but not the spirit of open source.

Veolia : Brings up the question of governance, you need someone to drive or
lead the community.

Amel : open source manager anyone ?

Brad : we have hired someone, but it's only inside the company. It's a paid
job, it's not some kind of volunteer position.

Société générale : evangelist kind of role internal, does not need to be about
technical culture. "sharing is scary" kind of thing

SG Femme : redondance entre projets. Using github, open their repository. it's
not about money, but attractiveness for open source developers. Maybe with some
vendors, leverage to negotiate contracts and prevent them from raising prices.

Cozy : c'est compliqué, ils ne font que des levées de fond. C'est un peu le
nextcloud avec croisement des données. On a un agrégateur bancaire pour gérér
les factures quand on visualise son compte en banque. Mais on ne revend pas des
données => [Amel: c'est d'habitude un des modèles possibles qui là est bloqué].
