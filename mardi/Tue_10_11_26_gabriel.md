From components to commerce to connections: open source at Huawei, Bryan Che,
CSO Huawei

[pub bluemind : ça vaut le coup d'aller la voir, il est question d'open source
et de souveraineté vs Outlook, c'est assez cringe aussi]

*Open source at Huawei*.

2009, first Android Phone from Huawei.
2010 : decide to enter enterprise and cloud computing space. Built on top of
open technnologies. Linaro.

Components. Open source => parce qu'ils décident de se lancer dans le cloud
space, entreprise business.

Commerce. Montre comment Huawei devient l'un des top sponsors and contributors
to open source. A *major journey*.

Connections :

Cloud computing is going to go away, we have to go into the future. 2 big
trends :

- Artificial Intelligence (most of the workload, 80% of the data center
    capacity in 2025)
- Edge cloud : devices at the edge (téléphones, etc) : comment envoyer les
    informations du datacenter aux téléphones ; comment utiliser les edge
    devices pour faire du Machine Learning

All layers in the open.

Ils ont beaucoup travaillé pour vori comment utiliser ARM devices poru faire du
machine learning => power gains (parce que ARM plus économe). [Intéressant
parce qu'en fond de trame : difficultés majeures d'Intel actuellement à la fois
techniquement et dans l'OS]

Slide super intéressante sur tout le stack [hardware -> cloud] et liens entre
partenaires et Huawei + place de l'OS. En gros seul le software est open
source et surtout plus c'est open source, moins Huawei s'investit (plus de
partenaires).

HarmonyOS microkernel [donc différent de Linux], distributed OS for machine
learning.

Slide : Intéressant beaucoup de gros chifres ici, même une évaluation de la
valeur du code contribué à Linux durant la réalisation de ce projet [12
milliards ???].

Open source : critical role in the global economy, esp for Europe in terms of
governance and data.

Eclipse Foundation : puissante en Europe, Huawei est devenue un parteraire au
plus haut niveau (ie ils ont donné le max d'argent).
