Inner source et la gouvernance open source : Florent Zara, Engie

Florent Zara, Engie Digital, ex-GPF Suez

*Innersource* inside Engie digital.

"Engie is a not only a lady, it's a big lady" [quelques rires type soufflement
de nez dans l'assistance, surtout de la part des speakers Orange et M$]

Innersource => utile pour des entreprises comme Engie qui est divisée en plein
d'entités légales.

[il n'explique pas l'innersource comme j'avais vu à github universe, il montre
juste une pyramide assez complexe]

Engie is always buying and selling companies, a lot of startups have their own
tools so Engie has to have the tools they use to put it in Innersource.

PayPal started Innersource, have written many books about it.

Attract skilled assets.

Peterson : they're not assets, they're people. [rires]

Be careful not to GPL v3 because it goes out otherwise. Licence compatible with
the closed source stuff.

They have a special Engie licence.

Les gens sont un peu confus sur le fait que c'est pas open source en fait,
plusieurs questions nécessaires pour clarifier ce point.

Peterson :

- précise distinction innersource / open source
- innersource issues : one of the issues, you have various laws around
  financing and financing different entities (anti competitive laws). Complex
  with different legal entities.

Les gens rigolent quand ils comprennent que c'est pas de l'OS, c'est juste
appliquer les "best practices" de l'OS.

Communication : github is not enough [because it's not suited for managers =>
build internal tool for that].
