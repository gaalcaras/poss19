La gouvernance open source au service de l'entreprise : Christian Paterson,
Orange

Christian Peterson, Orange
*La gouvernance open source au service de l'entreprise*

[présentation en EN]

OSS governance benefits business.

People associate governance with negative things => associate it with positive
things.

The world is becoming a digital colony [...] which is cool.

Orange sees OS as an enabler. Costs.

Quality
Sustainability
Digital Sovereignty
Economic Efficiency
Agility
Social Catalyst

"Anyone who says that OS is a technical thing [fais un bruit de bouche pour
dire "c'est pas bien"], out! OS is a human thing."

"Developers love this stuff".

You're using OS whether you know it or not.

It's not just pushing on Github : you're missing half of it.

Important : to business align (?)

"Hopefully you know that open source are projects they're not products by
themselves, you have to create the business around it" => "why are you doing
open source?"

"#1 reason nowadays" => attract the best developers.

OS => Part of the successful execution of your industrial strategy.

Important to talk to you guys, "and girls of course".
