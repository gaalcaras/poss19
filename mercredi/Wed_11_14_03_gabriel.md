Renforcer sa gestion des secrets avec des outils libres - Le cas d'XWiki SAS
: Clément Aubin, Xwiki sas

Clément Aubin, 25-30 ans, XWiki SAS depuis 2016

Comment ils ont restructuré leur stratégie de gestion des mots de passe,
Passbolt (OS solution pour les mpd) + les outils qu'ils ont créé autour de ce
projet.

Deux sources de mdp :

- internes (comptes en banque, comptes marketing, infra)
- externes (des clients, pour les instances XWiki)

Permettre de créer des connecteurs / modules.

Question : audit du code ?

R : pas d'audit
