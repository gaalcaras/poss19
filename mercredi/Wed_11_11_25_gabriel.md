Sur la courbe d’apprentissage de la gouvernance open source : Franck Villaume,
Microsoft

En fait c'est Frédéric Aatz (?), Microsoft France

[thinkpad avec stickers microsoft <3 linux etc]

[une 20aine de personnes dans la salle, jauge 30, 2 femmes]

*Open source Governance Learning Curve*

Just one slide is MS, so we agree with what Christian just shared. We need to
behave as good citizens there. Share our experiences there.

This is a thing that young people are looking for when going into a company.
Recognizing the value a star contributor has to the bottom line and the
shareholder, eventually.

Parle de l'initiative OW2 qui n'est pas encore complètement lancée.

At Microsoft, most of the work on OS has been done in the US but not so much in
France and US for now.

Le mec demande aux gens dans la salle de quoi ils auraient besoin ?

- H : Cheatsheet juridique
- Peterson : comment présenter l'open source aux shareholders, aux gros boss
  qui ne comprennent pas toujours l'intérêt. Traduire en termes économiques.
  "Help our business colleagues to better understand how it fits into their
  business model". Compliance is also a big challenge.
