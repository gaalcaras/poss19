Pourquoi faire le choix de l'infrastructure ouverte ? : Thierry Carrez,
OpenStack Foundation (OSF)

Thierry Carrez

OpenStack Foundation

[Salle 50 personnes, on est 5 dans l'assistance, tous des hommes, 3 de mon âge,
3 plus vers 45 ans. Il fait le talk en anglais même si tout le monde parle
français dans la salle parce qu'il a l'habitude de le faire en anglais]

Trends :

- Devs don't want to care about infrastructures anymore
- Commoditizing hardware : scale out vs scale up
- Commoditizing runtime env : cattle vs pets (easily kill and retry from scratch)
- Building more and more layers in the infrastructure stack => more and more
    opportunity for providing infrastructure => that's the opportunity for open
    source

Key benefits from OS software, even though it can be easy to overlook them

- Availability (don't have overhead, don't have to ask permission, no friction
    from going to experimentation to production)
- Sustainability : avoid vendor lock-in
- Influenceability : possibility to engage in the community and influence it
    for the organizations

3 Cs :

- Capabilities : one size does not fit all, customization (vs AWS)
- Compliance : legal requirements around data locality (because of strong
    privacy laws) and confidentiality around strategic companies.
- Cost : save massive amounts in public maintenance

Interoperability :

Hybrid cloud is clearly an industry buzzword, but there's a reason for it.
Difference between public cloud (price stays the same as you scale it) vs
private cloud (price decreases with economies of scale) => but in reality, your
usage is not constant, but you have peaks and lows. So cover your lower base
with private cloud, and then pay for the extra when you hit a peak with public
cloud.

Open infrastructure enables innovation : avoid monopolies and monocultures.

Blabla enable everyone : enable Africa [la slide avec des mains d'enfants noirs
= top], cheap, etc.

Question : intérêt d'utiliser est clair, mais contribuer ? Est-ce vraiment un
intérêt ?

Intervenant : problème car les personnes qui pourraient contribuer à openstack
sont des gens dont c'est pas le métier d'écrire du code, ils utilisent des
infrastructures. Les plus gros contributeurs extérieurs sont les intégrateurs
=> RedHat surtout.

Question : donc le problème c'est la communauté

[tout le monde] : oui oui c'est la **communauté**.

Intervenant : mais l'avantage, c'est que même si ça s'arrête tu peux continuer.
Exemple avec Suse qui a récemment abandonné sa solution basée sur OpenStack =>
les clients peuvent quand même continuer à l'utiliser et se tourner vers
OpenStack.

Pour les contributeurs : il faut réussir à les convaincre de l'intérêt de
contribuer. Le principal : tout le monde patche dans son coin et maintien un
gros delta dans son coin plutôt que de contribuer upstream parce que c'est
compliqué. Or maintenir un gros delta dans son coin, ça devient compliqué :
accumulation de dette technique, super complexe, effets imprévus, ça peut
casser... Au bout de quelques années ça devient évident.

