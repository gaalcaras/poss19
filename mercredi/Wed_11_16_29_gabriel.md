Personnalisation de services Web, vie privée et chiffrement : retour
d'expérience : Levent Demir, Qwant

A terminé sa thèse en 2017 sur le chiffrement côté client.

Peut-on respecter la vie privée tout en personnalisant ?

Ne veut pas faire trop de pub sur Qwant mais plutôt "prendre de la hauteurde la
hauteur"

Plusieurs scénarios de chiffrement :

- pas de chiffrement (sauf en transit)
- chiffrement une fois arrivé côté serveur
- chiffrement côté client et donnée chiffrée stockée serveur
- donnée entièrement chiffrée et stockée côté client, plus rien sur le serveur

Normalement, données côté serveur permettent de faire la personnalisation côté
serveur => "problème de privacy".

Comment faire de la reco en préservant privacy :

- tout client side, possible mais plus il y a de choix plus c'est difficile

Problème : comment synchroniser entre appareils ?

- Serveur "passe-plat" qui ne fait que passer les données
- webrtc pour faire du P2P avec les appareils

Autre problème : si on perd son (ou un) appareil, les données sont perdues pour
toujours ?

En gros : cette situation va bien si l'utilisateur est avancé et connaît ces
questions, mais pour le grand public c'est pas possible.
