EU-FOSSA: Projet pilote en matière de sécurité des logiciels libres dans les
institutions de l’UE, Marek Przybyszewski & Saranjit ARORA, European Commission - DIGIT

Open source is used by the European Union. Sometimes it's difficult to
appreciate the value it adds. "Obviously it costs less", "at least a billion
dollars to the EU economy", ...

"As in many companies, Open source crept in, the management didn't decide to
use it"

Bug bounties program. Mais c'est compliqué (surtout pour le financement,
à cause des règles EU, pas assez de flexibilité)

Hackathons [initiative Saranjit] => nouvelle forme de financement. Hackathon
symfony => ont payé tous les devs à venir à Bruxelles pour un week-end => ont
fermé 100 issues.

Vidéo institutionnelle sur les hackathons. Clairement c'est une publicité pour
encourager les projets à postuler à cette initiative.

Report : updated open source strategy report of EU. Firefox, VLC, Java, ...

Intéressant : Saranjit explique qu'il était super supris de voir que les gens
voulaient continuer à travailler le soir alors que le hackathon était censé
s'arrêter : "if we can suck that energy out, we can do wonders"
