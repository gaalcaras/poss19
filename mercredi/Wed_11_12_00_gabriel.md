Gouvernance open source et bonnes pratiques : l’exemple de la MAIF : François
Desmier, MAIF

François Desmier, MAIF [T-Shirt otoroshi => leur outil OS maison, voir keynote
MAIF]

[maintenant on est salle comble, 30 dont 5F]

700 employees big company but not a technology one.

maif.github.io

[il parle en anglais mais il galère beaucoup]

Don't forget that open source is first about the code and people who write it
=> they are the key ressource.

[c'est laborieux, il cherche beaucoup ses mots, les speakers précédents
[beaucoup plus âgés, et aussi probablement plus des cadres que des devs] lui
proposent des mots. Pas mal de gens s'en vont, environ 5, et c'est midi en plus]

Importance des conférences pour représenter la marque MAIF sur la scène
technique [j'interprète car son anglais est trop haché]. En interne, ils ont
des ateliers tous les mois pour encourager et entraîner les dévs à parler puis
les envoyer en conférence.

You have to educate managers that developers can contribute to other projects
than the companies'.
