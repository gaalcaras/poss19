Et si l’Open Source était la clé de la cybersécurité des données ? : Thierry
Leblond, Scille

Spécialisé dans le cloisonnement de la donnée sur le cloud.

Partenaires : ministère des armées (lui a dirigé la vidéossurveillance pour la
mairie de Paris aussi), université de Bordeaux, CEA.

Open Source => avantage pour la sécurité => le seul secret à protéger devient
les clés de chiffrement.

Sécurité authentification (on sait faire), transport (https, on sait faire),
partage (là on sait pas faire).

Avec une employée [seule personne noire que j'ai croisée jusqu'à présent], une
démo très longue et pas très concluante, beaucoup d'aller-retours].
